#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int main() {
	srand(time(NULL));

	const int N = 11;
	int arr[N];
	int *p = arr;

	for (int i = 0; i < N; i++) {
		arr[i] = -100 + rand() % 200;
		printf("%3d | ", arr[i]);
	}


	int sizeOfArray = sizeof(arr);
	printf("\nsizeOfArray = %d", sizeOfArray);

	int countOfElements = sizeOfArray / sizeof(arr[0]);
	printf("\ncountOfElements = %d\n", countOfElements);

	printf("&arr[0] = %d\n&arr[N-1] = %d\n", &arr[0], &arr[N - 1]);

	int* endP = p + countOfElements - 1;

	while (p < endP) {
		int temp = *p;
		*p = *endP;
		*endP = temp;
		p++;
		endP--;
	}

	for (int i = 0; i < N; i++) {
		printf("%3d | ", arr[i]);
	}

	return 0;
}