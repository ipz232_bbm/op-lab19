#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int main() {
	srand(time(NULL));

	int SIZE = 13, pSIZE = SIZE;;
	int* p = NULL;

	p = (int*)calloc(SIZE, sizeof(int));

	for (int i = 0; i < SIZE; i++) {
		p[i] = -100 + rand() % 200;
		printf("%3d | ", p[i]);
	}
	printf("\n");
	for (int k = 1; k <= (SIZE - 1) / 3; k++) {
		for (int i = 3 * k - k + 1; i < pSIZE; i++) p[i] = p[i + 1];
		pSIZE--;
	}

	realloc(p, pSIZE * sizeof(int));

	for (int i = 0; i < pSIZE; i++) printf("%3d | ", p[i]);

	int countNegative = 0;
	for (int i = 0; i < pSIZE; i++) {
		if (p[i] < 0) countNegative++;
	}
	pSIZE += countNegative;
	realloc(p, pSIZE * sizeof(int));
	printf("\n");
	for (int i = 0; i < pSIZE; i++) {
		if (i >= pSIZE - countNegative) p[i] = 0;
	}
	printf("\n");
	
	for(int g = 0; g < countNegative; g++)
	for (int k = countNegative - g; k > 0;) {
		for (int i = pSIZE - 1;; i--) {
			if (p[i - 1] < 0 && k == 1) {
				p[i] = p[i - 1];
				k--;
				break;
			}
			else if (p[i - 1] < 0) {
				k--;
			}
			p[i] = p[i - 1];
		}
	}

	for (int i = 0; i < pSIZE; i++) {
		if (p[i] < 0) {
			p[i + 1] = p[i] + 1;
			i++;
		}
	}


	for (int i = 0; i < pSIZE; i++) printf("%3d | ", p[i]);

	return 0;
}