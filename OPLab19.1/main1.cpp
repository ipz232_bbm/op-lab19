#include <stdio.h>

int main() {
	int* p;

	int x = 10, y = 5, m[10];

	p = &y;

	printf("*p (y) = %d\n", *p);

	x = *p;

	printf("x = %d\n", x);

	y = 7;

	printf("y = %d\n", y);

	*p += 5;

	printf("y = %d", y);

	return 0;
}