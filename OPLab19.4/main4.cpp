#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int main() {
	srand(time(NULL));

	int n;
	do {
		printf("n( n > 0) = ");
		scanf("%d", &n);
	} while (n <= 0);
	int* arr = (int*)calloc(n, sizeof(int));

	for (int i = 0; i < n; i++) {
		arr[i] = -100 + rand() % 200;
		printf("%d | ", arr[i]);
	}

	for (int i = 0; i < n - 1; i += 2) {
		int temp = arr[i];
		arr[i] = arr[i + 1];
		arr[i + 1] = temp;
	}
	printf("\n");
	for (int i = 0; i < n; i++) {
		printf("%d | ", arr[i]);
	}
	return 0;
}